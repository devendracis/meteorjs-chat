Template.signin.events({
    'submit form': function(event) {
        event.preventDefault();
       var emailVar = $("input#form-email").val();
	  var passwordVar = $("input#form-password").val();
	  Meteor.loginWithPassword(emailVar, passwordVar, function(err){
	  	if(err){
	  		console.log(err)
	  	}else{
        	//Router.go('welcome');
            window.location = "/welcome/"+Meteor.userId();
	  	}
	  });
    }
});

Template.registerHelper( 'userId', () => {
  
  if (Meteor.userId()) {
    return Meteor.userId();
  }
});