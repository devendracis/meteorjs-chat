Template.welcome.events({
  'click #log-out' : function (event) {
  	event.preventDefault();
  	Meteor.logout();
  	Router.go('/signIn');
  }
});
