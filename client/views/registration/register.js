Template.register.events({
  'submit form': function(event) {
    event.preventDefault();
    var userName = /^\w+$/, 
    passwordNumberValidation = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    if (!userName.test($("input#form-username").val())) {
      Bert.alert("Username must contain only letters, numbers and underscores!",'danger');
    } else if (!passwordNumberValidation.test($("input#form-password").val())) {
       Bert.alert("Password should be contains capital letter, small letter and numbers", 'danger');
    } else if ($("input#form-password").val() == $("input#form-username").val()) {
      Bert.alert("Password must be different from Username!");
    } else if ($("input#form-password").val() != $("input#form-confirm-password").val()) {
      Bert.alert("Please check that you've entered and confirmed your password!",'danger');
    } else
      var user = {
          email: $("input#form-email").val(),
          password: $("input#form-password").val(),
          username: $("input#form-username").val()
      }
      Accounts.createUser( user, ( error ) => {
        if (error) {
          Bert.alert( error.reason, 'danger' );
        } else {
          Meteor.call( 'sendVerificationLink', ( error, response ) => {
            if (error) {
              Bert.alert( error.reason, 'danger' );
            } else {
              Bert.alert( 'Please verify link to activate your account', 'success' );
              Router.go('/signIn');
            }
          });
        }
      });
        // Router.go('/welcome');
  }
});
