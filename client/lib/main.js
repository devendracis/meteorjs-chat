setTitle = (title) => {
  let base = 'G-Chat';
  if (title) {
    document.title = title + ' - ' + base;
  }
};
