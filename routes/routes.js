Router.route('/signIn', {
    name: 'signin',
    action: function(){
      setTitle('SignIn');
      this.render('signin');
    }
  });


Router.route('/', {
  name: 'register',
  action: function () {
    setTitle('Register');
    this.render('register');
  }
});

Router.route('/welcome/:profileId', {
    'name': 'welcome',
    'template': 'welcome'
  });

Router.route('/home', {
    name: 'home',
    action: function(){
      this.render('home');
      setTitle('Home');
    }
  });

Router.route('/profile/:profileId', {
    name: 'profile',
    action: function(){
      setTitle('Profile');
      this.render("profile")
    }
  });

Router.route('/message', {
    name: 'message',
    action: function(){
      setTitle('chat');
      this.render("message")
    }
  });

Router.route('/verify-email/:token', {
  'name': 'verifyEmail',
  'template': 'verifyEmail'
});